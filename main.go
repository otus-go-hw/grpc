package main

import (
	"log"

	"gitlab.com/otus-go-hw/grpc/cmd"
)

func main() {
	if err := cmd.RootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}
